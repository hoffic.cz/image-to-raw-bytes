module cz.hoffic.imagetorawbytes {
  requires javafx.base;
  requires javafx.controls;
  requires javafx.graphics;

  opens cz.hoffic.imagetorawbytes;
}
